# Python Rest Server Tictactoe

A simple Python flask server that plays tic-tac-toe against you via a REST api. Player is always 'O' and the server is always 'X'.  Each time the player makes a move the server will attempt to make a move immediately after. First player to get three in a row wins. Board is designated as such:
```
[0] [1] [2]
[3] [4] [5]
[6] [7] [8]
```

## Installation

Download this repo and install dependencies:
```
git clone https://gitlab.com/noroff-accelerate/embedded-bootcamp/projects/python-rest-server-tictactoe
cd python-rest-server-tictactoe
pip install -r requirements.txt
```

## Usage

To run this server simply run the following command:
```
python3 src/main.py
```
This will deploy a Flask server that will listen on 
```
http://127.0.0.1:5000
```
by default.

## REST API specification

The server has three endpoints:

| URL     | Method  | Required Header | Success | Error | Purpose |
|:-------:|:-------:|-----------------|:-------:|:-----:|:--------|
|`/`      |`GET`      | |`200`    | | A simple `GET` end point to test if the server is running succesfully |
|`/board` |`GET`      | |`200`    | | Returns the state of the TicTacToe board as a JSON object             |
|`/play`  |`POST`     | `application/json`| `200` | `400` `403` `405` | Allows the player to submit a valid play. The play must be in the form of a `POST` request with a JSON object payload using the format: `{ "move": X }` where X can be any int in the range from `0-8` representing the board position. The move can only be played in an empty board position. A success will either yield a move accepted message or a declared winner. If a winner is determined the board will reset immediately. |

## Examples
Below is are examples of using the end points successfully
```
$ curl http://127.0.0.1:5000/
> Tic Tac Toe bot here! Ready to play.
```

```
$ curl http://127.0.0.1:5000/play -X POST -H "Content-Type: application/json" -d "{ \"move\": 4 }"
> Move accepted
```

```
$ curl http://127.0.0.1:5000/board
> {"0": null, "1": null, "2": "x", "3": null, "4": "o", "5": null, "6": null, "7": null, "8": null}
```


## Maintainers 

JC Bailey - @thelunararmy

## License 

MIT © 2022 Noroff Accelerate


