from flask import Flask, request, Response
from random import choice
import json

### TIC TAC TOE LOGIC
board = { x:None for x in range(9) }

def reset_board():
    global board
    board = { x:None for x in range(9) }

def play_move(position,player):
    global board
    if (position not in board or board[position] is not None):
        return -1
    else: 
        board[position] = player
        return 1

def print_board():
    global board 
    to_print = [x if x is not None else " " for x in board.values() ]
    print(f"[{to_print[0]}] [{to_print[1]}] [{to_print[2]}]")
    print(f"[{to_print[3]}] [{to_print[4]}] [{to_print[5]}]")
    print(f"[{to_print[6]}] [{to_print[7]}] [{to_print[8]}]")

def check_win():
    global board
    combinations = [
        {0,1,2}, {3,4,5}, {6,7,8}, # rows
        {0,3,6}, {1,4,7}, {2,5,8}, # columns
        {0,4,8}, {2,4,6} # diagonals
    ]
    x_positions = set([ k for k,v in board.items() if v == 'x' ])
    o_positions = set([ k for k,v in board.items() if v == 'o' ])
    
    for c in combinations:
        if c.intersection(x_positions) == c:
            return "x wins"
        if c.intersection(o_positions) == c:
            return "o wins"   
    
    if len(x_positions.union(o_positions)) == 9:
        return "game is a draw"

    return None

def player_play(position):
    result = play_move(position,"o")
    if result < 0: 
        raise ValueError("Illegal Move")
    return position

def available_moves():
    global board
    return [ k for k,v in board.items() if v is None ]

def bot_play():
    global board
    move = choice(available_moves())
    play_move(move,"x")
    return move

def debug_play():
    global board
    reset_board()
    while(True):
        # Bot gets a turn
        bot_play()
        print_board()
        result = check_win()
        if result is not None:
            print(f"Game over: {result}!")
            break
        # Player gets a turn
        player_play(int(input(f"Pick a move, available: {[ k for k,v in board.items() if v is None ]} ")))
        print_board
        result = check_win()
        if result is not None:
            print(f"Game over: {result}!")
            break

### FLASK LOGIC
app = Flask(__name__)

@app.route("/")
def index():
    return 'Tic Tac Toe bot here! Ready to play.'

@app.route("/board")
def give_board():
    global board
    return json.dumps(board)

@app.route("/play", methods=['POST'])
def play():
    # Fetch json data from message
    post_json = request.get_json()
    if post_json is None:
        return Response(status=400,response="POST content must contain a JSON object")

    # Fetch move from json
    move = post_json.get("move",None)
    if move is None:
        return Response(status=400,response="Request JSON object must contain 'move'")
    
    # Ensure move is acceptable
    try:
        move = int(move)
    except Exception:
        return Response(status=400,response="Request 'move' must be numeric")

    # Enesure the player CAN play the move
    if (move not in available_moves()):
        return Response(status=403, response="Illegal Move")

    # Let the game progress
    player_play(move)
    result = check_win()
    if result is not None:
        reset_board()
        return Response(status=200, response=f"Game over! {result}")
    bot_play()
    result = check_win()
    if result is not None:
        reset_board()
        return Response(status=200, response=f"Game over! {result}")

    # If no response has been sent yet then all moves were legal
    return Response(status=200, response="Move accepted.")

if __name__ == '__main__':
    app.run(debug=True)









